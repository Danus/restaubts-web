FROM node:latest
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build:ssr
EXPOSE 4200
ENV PORT="4200"
ENV VIRTUAL_HOST="restaubts.superbock.fun"
ENV LETSENCRYPT_EMAIL="vieiradaniel1998@gmail.com"
ENV LETSENCRYPT_HOST="restaubts.superbock.fun"
ENV DBPORT="3306"
ENV DBHOST="172.17.0.4"
ENV DBUSER="root"
ENV DBPASSWORD="schoolpw"
ENV DBNAME="restauBTS"
CMD [ "node", "dist/restau-bts/server/main.js" ]
