import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	constructor(private cookieService: CookieService) {}

	private isAuthenticated(): boolean {
		return this.cookieService.get('loginTokenRestauBTS') !== null;
	}

	private getAuthToken(): string {
		return this.cookieService.get('loginTokenRestauBTS');
	}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// add authorization header with jwt token if available
		if (this.isAuthenticated()) {
			const token = this.getAuthToken();
			request = request.clone({
				setHeaders: {
					Authorization: `Bearer ${token}`,
				},
			});
		}

		return next.handle(request);
	}
}
