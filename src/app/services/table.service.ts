import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TableArrayDto, TableDbDto, TableDto} from '../../../server/src/table/table.dto';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private http: HttpClient) {}

  private url = '/api/table/';

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  public getAll(): Observable<TableArrayDto> {
    return this.http.get<TableArrayDto>(this.url + 'getAll', this.httpOptions);
  }

  public get(id: number): Observable<TableDbDto> {
    return this.http.get<TableDbDto>(this.url + 'get/' + id, this.httpOptions);
  }

  public add(tableNr: number, places: number) {
    const table: TableDto = {
      tableNr: tableNr,
      places: places,
    };
    return this.http.post<TableDto>(this.url + 'create', table, this.httpOptions);
  }

  public update(tableNr: number, places: number, id: number) {
    const table: TableDto = {
      tableNr: tableNr,
      places: places,
    };
    return this.http.post<TableDto>(this.url + 'update/' + id, table, this.httpOptions);
  }

  public delete(id: number): Observable<string> {
    return this.http.delete<string>(this.url + id);
  }
}
