import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DbBillDto} from '../../../server/src/bill/bill.dto';

@Injectable({
  providedIn: 'root'
})
export class BillService {

  constructor(private http: HttpClient) {}

  private url = '/api/bill/';

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  public getAll(): Observable<DbBillDto[]> {
    return this.http.get<DbBillDto[]>(this.url + 'getAll', this.httpOptions);
  }
}
