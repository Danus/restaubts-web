import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserDto, UserPasswordChangeDto} from '../../../server/src/user/user.dto';
import {Observable} from 'rxjs';
import {User} from '../components/user-table/user-table.component';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) {}

    private url = '/api/user/';

    private httpOptions = {
        headers: new HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        })
    };

    public login(username: string, password: string) {
        const loginCredentials: UserDto = {
            username: username,
            password: password,
            role: ''
        };
        return this.http.post<UserDto>(this.url + 'login', loginCredentials, this.httpOptions);
    }

    public register(username: string, password: string, role: string) {
        const loginCredentials: UserDto = {
            username: username,
            password: password,
            role: role
        };
        return this.http.post<UserDto>(this.url + 'register', loginCredentials, this.httpOptions);
    }

    public changePassword(userPasswordChangeDto: UserPasswordChangeDto) {
        return this.http.post<UserPasswordChangeDto>(this.url + 'changePassword', userPasswordChangeDto, this.httpOptions);
    }

    public getAll(): Observable<User[]> {
        return this.http.get<User[]>(this.url + 'getAll', this.httpOptions);
    }

    public get(username: string): Observable<User> {
        return this.http.get<User>(this.url + 'get/' + username, this.httpOptions);
    }

    public delete(username: string): Observable<string> {
        return this.http.delete<string>(this.url + username);
    }

    public update(username: string, password: string, role: string): Observable<boolean> {
        const loginCredentials: UserDto = {
            username: username,
            password: password,
            role: role
        };
        return this.http.post<boolean>(this.url + 'update', loginCredentials, this.httpOptions);
    }
}
