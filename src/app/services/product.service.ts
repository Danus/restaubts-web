import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DbProductBillDto, DbProductDto, ProductArrayDto, ProductDto} from '../../../server/src/product/product.dto';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  private url = '/api/product/';

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  public getAll(): Observable<ProductArrayDto> {
    return this.http.get<ProductArrayDto>(this.url + 'getAll', this.httpOptions);
  }

  public getByBill(billId: number): Observable<DbProductBillDto[]> {
    return this.http.get<DbProductBillDto[]>(this.url + 'getByBill/' + billId, this.httpOptions);
  }

  public get(id: number): Observable<DbProductDto> {
    return this.http.get<DbProductDto>(this.url + 'get/' + id, this.httpOptions);
  }

  public add(name: string, category: string, value: number, image: string) {
    const product: ProductDto = {
      productNr: null,
      name: name,
      category: category,
      value: value,
      image: image
    };
    return this.http.post<ProductDto>(this.url + 'create', product, this.httpOptions);
  }

  public update(productNr: number, name: string, category: string, value: number, image: string) {
    const product: ProductDto = {
      productNr: productNr,
      name: name,
      category: category,
      value: value,
      image: image
    };
    return this.http.post<ProductDto>(this.url + 'update', product, this.httpOptions);
  }

  public delete(id: number): Observable<string> {
    return this.http.delete<string>(this.url + id);
  }
}
