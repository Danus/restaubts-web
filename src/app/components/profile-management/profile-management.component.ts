import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {JwtHelper} from '../../jwt.helper';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {User} from '../user-table/user-table.component';

@Component({
    selector: 'app-profile-management',
    templateUrl: './profile-management.component.html',
    styleUrls: ['./profile-management.component.scss']
})
export class ProfileManagementComponent implements OnInit {
    public username: string;
    public updateUser: any;
    public errorMessage: string;
    public successMessage: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private cookieService: CookieService,
        private formBuilder: FormBuilder,
        private userService: UserService
    ) {
    }

    ngOnInit(): void {
        this.initFormBuilder();
        const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
        // if user is not logged in, redirect to login
        if (token.role !== 'admin') {
            this.router.navigateByUrl('/login').then();
        } else {
            this.username = this.route.snapshot.paramMap.get('username');
            this.getAllUserData();
        }
    }

    private getAllUserData() {
        this.userService.get(this.username).subscribe(
            data => this.setUserData(data),
            error => console.log(error),
        );
    }

    private setUserData(user: User) {
        this.updateUser.controls.role.setValue(user.dtRole);
    }

    private initFormBuilder() {
        this.updateUser = this.formBuilder.group(
            {
                password: [''],
                role: ['', Validators.compose([Validators.required])],
            },
        );
    }

    public update(): void {
        if (this.updateUser.valid) {
            this.successMessage = '';
            this.errorMessage = '';

            this.userService
                .update(
                    this.username,
                    this.updateUser.get('password').value,
                    this.updateUser.get('role').value
                ).subscribe(
                _ => (this.router.navigate(['/users']).then()),
                error => (this.errorMessage = error.error.message),
            );
        }
    }
}
