import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillProductTableComponent } from './bill-product-table.component';

describe('BillProductTableComponent', () => {
  let component: BillProductTableComponent;
  let fixture: ComponentFixture<BillProductTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillProductTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillProductTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
