import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {DbProductBillDto} from '../../../../server/src/product/product.dto';
import {ProductService} from '../../services/product.service';
import {CookieService} from 'ngx-cookie-service';
import {ActivatedRoute, Router} from '@angular/router';
import {JwtHelper} from '../../jwt.helper';

@Component({
  selector: 'app-bill-product-table',
  templateUrl: './bill-product-table.component.html',
  styleUrls: ['./bill-product-table.component.scss']
})
export class BillProductTableComponent implements OnInit {

  public products: MatTableDataSource<DbProductBillDto>;
  public displayedColumns = ['productId', 'name', 'quantity', 'value'];
  public billId: number;
  public billTotal = 0;

  constructor(
      private productService: ProductService,
      private cookieService: CookieService,
      private router: Router,
      private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
    if (token.role === 'admin') {
      if (typeof (window) !== 'undefined') {
        this.billId = Number(this.route.snapshot.paramMap.get('billId'));
        this.getAllProducts();
      }
    } else {
      this.router.navigateByUrl('/').then();
    }
  }

  private getAllProducts() {
    this.productService.getByBill(this.billId).subscribe(
        data => this.setProducts(data),
        error => console.log(error),
    );
  }

  private setProducts(products: DbProductBillDto[]) {
    this.products = new MatTableDataSource(products);

    products.forEach((product) => {
      this.billTotal += product.dtValue * product.dtQuantity;
    });

    this.products.filterPredicate = function(data, filter: string) {
      return data.dtName.toLowerCase().includes(filter) || data.idProduct.toString().includes(filter);
    };
  }

  public applyFilter(filterValue: String) {
    this.products.filter = filterValue.trim().toLowerCase();
  }

}
