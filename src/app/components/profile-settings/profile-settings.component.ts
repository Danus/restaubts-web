import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {CustomValidators} from './custom-validators';
import {JwtHelper} from '../../jwt.helper';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
    selector: 'app-profile-settings',
    templateUrl: './profile-settings.component.html',
    styleUrls: ['./profile-settings.component.scss']
})
export class ProfileSettingsComponent implements OnInit, OnDestroy {

    private updateSubscription: Subscription;
    private token: any;
    public updateSettings: FormGroup;
    public errorMessage: string;
    public successMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        private cookieService: CookieService,
        private router: Router,
        private userService: UserService
    ) {
    }

    ngOnInit(): void {
        this.token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
        if (this.token.username === undefined) {
            this.router.navigateByUrl('/login').then();
        }

        this.initFormBuilder();
    }

    ngOnDestroy(): void {

    }

    private initFormBuilder() {
        this.updateSettings = this.formBuilder.group(
            {
                oldPassword: ['', Validators.compose([Validators.required])],
                password: ['', Validators.compose([Validators.required])],
                confirmPassword: [''],
            },
            {
                // check if password and confirm password matches
                validator: CustomValidators.passwordMatchValidator,
            },
        );
    }

    private updateSuccess() {
        this.updateSettings.controls.oldPassword.setValue('');
        this.updateSettings.controls.password.setValue('');
        this.updateSettings.controls.confirmPassword.setValue('');
        this.initFormBuilder();
        this.successMessage = 'Your password has been updated!';
    }

    // update profile
    public update(): void {
        if (this.updateSettings.valid) {
            this.successMessage = '';
            this.errorMessage = '';

            this.updateSubscription = this.userService
                .changePassword({
                    newPassword: this.updateSettings.get('password').value,
                    oldPassword: this.updateSettings.get('oldPassword').value,
                    username: this.token.username
                }).subscribe(
                    _ => (this.updateSuccess()),
                    error => (this.errorMessage = error.error.message),
                );
        }
    }
}
