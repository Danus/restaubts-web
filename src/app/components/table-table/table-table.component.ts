import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {JwtHelper} from '../../jwt.helper';
import {TableArrayDto, TableDbDto} from '../../../../server/src/table/table.dto';
import {TableService} from '../../services/table.service';

@Component({
  selector: 'app-table-table',
  templateUrl: './table-table.component.html',
  styleUrls: ['./table-table.component.scss']
})
export class TableTableComponent implements OnInit {

  public tables: TableDbDto[];
  public displayedColumns = ['tableNr', 'places', 'status'];
  public modalStatus: boolean;
  public addTable;
  public errorMessage: string;
  public deleteConfirm: boolean;
  public currentTable: number;
  public mode: string;

  constructor(
      private tableService: TableService,
      private cookieService: CookieService,
      private router: Router,
      private formBuilder: FormBuilder,
      private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.deleteConfirm = false;
    this.initFormBuilder();
    this.cdr.detectChanges();
    this.mode = 'Add';

    const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
    if (token.role === 'admin') {
      if (typeof (window) !== 'undefined') {
        this.getAllTables();
      }
    } else {
      this.router.navigateByUrl('/').then();
    }
  }

  private initFormBuilder() {
    this.addTable = this.formBuilder.group(
        {
          tableNr: ['', Validators.compose([Validators.required])],
          places: ['', Validators.compose([Validators.required])]
        }
    );
  }

  private getAllTables() {
    this.modalStatus = false;
    this.tableService.getAll().subscribe(
        data => this.setTables(data),
        error => console.log(error),
    );
  }

  private setTables(tables: TableArrayDto) {
    this.tables = tables.tables;
  }

  public performRequest(): void {
    if (this.addTable.valid) {
      this.errorMessage = '';

      if (this.mode === 'Add') {
        this.tableService
            .add(
                this.addTable.get('tableNr').value,
                this.addTable.get('places').value,
            ).subscribe(
            _ => (this.getAllTables()),
            error => (this.errorMessage = error.error.message),
        );
      } else if (this.mode === 'Update') {
        this.tableService
            .update(
                this.addTable.get('tableNr').value,
                this.addTable.get('places').value,
                this.currentTable
            ).subscribe(
            _ => (this.getAllTables()),
            error => (this.errorMessage = error.error.message),
        );
      }
    }
  }

  public openModal() {
    this.mode = 'Add';
    this.errorMessage = '';
    this.modalStatus = true;
    this.addTable.controls['tableNr'].setValue('');
    this.addTable.controls['places'].setValue('');
    this.initFormBuilder();
  }

  public openDeleteModal(tableNr: number) {
    this.deleteConfirm = true;
    this.currentTable = tableNr;
  }

  public deleteTable() {
    this.deleteConfirm = false;
    this.tableService.delete(this.currentTable).subscribe(
        _ => (this.getAllTables()),
        error => (console.log(error.message)),
    );
  }

  public modifyTable(tableNr: number, places: number) {
    this.currentTable = tableNr;
    this.mode = 'Update';
    this.errorMessage = '';
    this.modalStatus = true;
    this.addTable.controls['tableNr'].setValue(tableNr);
    this.addTable.controls['places'].setValue(places);
  }
}
