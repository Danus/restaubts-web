import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {JwtHelper} from '../../jwt.helper';
import {BillService} from '../../services/bill.service';
import {DbBillDto} from '../../../../server/src/bill/bill.dto';

@Component({
  selector: 'app-bill-table',
  templateUrl: './bill-table.component.html',
  styleUrls: ['./bill-table.component.scss']
})
export class BillTableComponent implements OnInit {

  public bills: DbBillDto[];
  public displayedColumns = ['id', 'date', 'tableNr', 'total'];

  constructor(
      private billService: BillService,
      private cookieService: CookieService,
      private router: Router,
  ) {
  }

  ngOnInit() {

    const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
    if (token.role === 'admin') {
      if (typeof (window) !== 'undefined') {
        this.getAllBills();
      }
    } else {
      this.router.navigateByUrl('/').then();
    }
  }

  private getAllBills() {
    this.billService.getAll().subscribe(
        data => this.setBills(data),
        error => console.log(error),
    );
  }

  private setBills(bills: DbBillDto[]) {
    this.bills = bills;
  }
}
