import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/internal/operators';
import {CookieService} from 'ngx-cookie-service';
import {JwtHelper} from '../../jwt.helper';
import {NavDropdown, NavItem} from '../../interfaces/navigation.interface';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})

export class NavigationComponent implements OnInit {
    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(result => result.matches),
        shareReplay(),
    );

    constructor(
        private breakpointObserver: BreakpointObserver,
        private cookieService: CookieService,
    ) {}

    public navItems: NavItem[];
    public dropDowns: NavDropdown[];

    private initNav(token: any) {
        if (token.role === 'user') {
            this.navItems = [
                {
                    name: 'Home',
                    path: '/',
                    icon: 'home',
                },
            ];
            this.dropDowns = [
                {
                    name: token.username,
                    icon: 'perm_identity',
                    items: [
                        {
                            name: 'Profile Settings',
                            path: '/profile',
                            icon: 'settings',
                        },
                        {
                            name: 'Logout',
                            path: '/logout',
                            icon: 'power_settings_new',
                        },
                    ],
                },
            ];
        } else if (token.role === 'admin') {
            this.navItems = [
                {
                    name: 'Home',
                    path: '/',
                    icon: 'home',
                },
                {
                    name: 'Tables',
                    path: '/tables',
                    icon: 'event_seat',
                },
                {
                    name: 'Menu',
                    path: '/products',
                    icon: 'room_service',
                },
                {
                    name: 'Bills',
                    path: '/bills',
                    icon: 'attach_money',
                },
            ];
            this.dropDowns = [
                {
                    name: token.username,
                    icon: 'person',
                    items: [
                        {
                            name: 'Users',
                            path: '/users',
                            icon: 'group',
                        },
                        {
                            name: 'Profile Settings',
                            path: '/profile',
                            icon: 'settings',
                        },
                        {
                            name: 'Logout',
                            path: '/logout',
                            icon: 'power_settings_new',
                        },
                    ],
                },
            ];
        } else {
            this.navItems = [
                {
                    name: 'Home',
                    path: '/',
                    icon: 'home',
                },
                {
                    name: 'Login',
                    path: '/login',
                    icon: 'person',
                },
            ];
            this.dropDowns = [];
        }
    }

    ngOnInit() {
        const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
        this.initNav(token);
    }
}
