import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {JwtHelper} from '../../jwt.helper';
import {DbProductDto, ProductArrayDto} from '../../../../server/src/product/product.dto';
import {ProductService} from '../../services/product.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent implements OnInit {

  public products: MatTableDataSource<DbProductDto>;
  public displayedColumns = ['productId', 'name', 'category', 'value'];
  public modalStatus: boolean;
  public addProduct;
  public errorMessage: string;
  public deleteConfirm: boolean;
  public currentProduct: number;
  public mode: string;

  constructor(
      private productService: ProductService,
      private cookieService: CookieService,
      private router: Router,
      private formBuilder: FormBuilder,
      private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.deleteConfirm = false;
    this.initFormBuilder();
    this.cdr.detectChanges();
    this.mode = 'Add';

    const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
    if (token.role === 'admin') {
      if (typeof (window) !== 'undefined') {
        this.getAllProducts();
      }
    } else {
      this.router.navigateByUrl('/').then();
    }
  }

  private initFormBuilder() {
    this.addProduct = this.formBuilder.group(
        {
          name: ['', Validators.compose([Validators.required])],
          category: ['', Validators.compose([Validators.required])],
          value: ['', Validators.compose([Validators.required])],
          image: ['', Validators.compose([Validators.required])]
        }
    );
  }

  private getAllProducts() {
    this.modalStatus = false;
    this.productService.getAll().subscribe(
        data => this.setProducts(data),
        error => console.log(error),
    );
  }

  private setProducts(productArray: ProductArrayDto) {
    this.products = new MatTableDataSource(productArray.products);
    this.products.filterPredicate = function(data, filter: string) {
      // tslint:disable-next-line:max-line-length
      return data.dtName.toLowerCase().includes(filter) || data.idProduct.toString().includes(filter) || data.dtCategory.toLowerCase().includes(filter);
    };
  }

  public applyFilter(filterValue: String) {
    this.products.filter = filterValue.trim().toLowerCase();
  }

  public performRequest(): void {
    if (this.addProduct.valid) {
      this.errorMessage = '';

      if (this.mode === 'Add') {
        this.productService
            .add(
                this.addProduct.get('name').value,
                this.addProduct.get('category').value,
                this.addProduct.get('value').value,
                this.addProduct.get('image').value,
            ).subscribe(
            _ => (this.getAllProducts()),
            error => (this.errorMessage = error.error.message),
        );
      } else if (this.mode === 'Update') {
        this.productService
            .update(
                this.currentProduct,
                this.addProduct.get('name').value,
                this.addProduct.get('category').value,
                this.addProduct.get('value').value,
                this.addProduct.get('image').value,
            ).subscribe(
            _ => (this.getAllProducts()),
            error => (this.errorMessage = error.error.message),
        );
      }
    }
  }

  public openModal() {
    this.mode = 'Add';
    this.errorMessage = '';
    this.modalStatus = true;
    this.addProduct.controls['name'].setValue('');
    this.addProduct.controls['category'].setValue('');
    this.addProduct.controls['value'].setValue('');
    this.addProduct.controls['image'].setValue('');
    this.initFormBuilder();
  }

  public openDeleteModal(productId: number) {
    this.deleteConfirm = true;
    this.currentProduct = productId;
  }

  public deleteProduct() {
    this.deleteConfirm = false;
    this.productService.delete(this.currentProduct).subscribe(
        _ => (this.getAllProducts()),
        error => (console.log(error.message)),
    );
  }

  public modifyProduct(productId: number, name: string, category: string, value: number, image: string) {
    this.currentProduct = productId;
    this.mode = 'Update';
    this.errorMessage = '';
    this.modalStatus = true;
    this.addProduct.controls['name'].setValue(name);
    this.addProduct.controls['category'].setValue(category);
    this.addProduct.controls['value'].setValue(value.toFixed(2));
    this.addProduct.controls['image'].setValue(image);
  }
}
