import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {UserService} from '../../services/user.service';
import {CookieService} from 'ngx-cookie-service';
import {JwtHelper} from '../../jwt.helper';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {CustomValidators} from '../profile-settings/custom-validators';

export interface User {
    idUserName: string;
    dtRole: string;
}

@Component({
    selector: 'app-user-table',
    templateUrl: './user-table.component.html',
    styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit, OnDestroy {

    private subscription: Subscription;
    public users: User[];
    public displayedColumns = ['username', 'role'];
    public modalStatus: boolean;
    public addUser;
    public errorMessage: string;
    public successMessage: string;
    public deleteConfirm: boolean;
    public currentUser: string;

    constructor(
        private userService: UserService,
        private cookieService: CookieService,
        private router: Router,
        private formBuilder: FormBuilder,
        private cdr: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        this.deleteConfirm = false;
        this.initFormBuilder();
        this.cdr.detectChanges();

        const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));
        if (token.role === 'admin') {
            if (typeof (window) !== 'undefined') {
                this.getAllUsers();
            }
        } else {
            this.router.navigateByUrl('/').then();
        }
    }

    ngOnDestroy(): void {
        if (typeof (window) !== 'undefined' && this.subscription !== undefined) {
            this.subscription.unsubscribe();
        }
    }

    private initFormBuilder() {
        this.addUser = this.formBuilder.group(
            {
                username: ['', Validators.compose([Validators.required])],
                password: ['', Validators.compose([Validators.required])],
                confirmPassword: [''],
                role: ['user', Validators.compose([Validators.required])],
            },
            {
                // check if password and confirm password matches
                validator: CustomValidators.passwordMatchValidator,
            },
        );
    }

    private getAllUsers() {
        this.modalStatus = false;
        this.subscription = this.userService.getAll().subscribe(
            data => this.setUsers(data),
            error => console.log(error),
        );
    }

    private setUsers(users: User[]) {
        this.users = users;
    }

    // add user
    public add(): void {
        if (this.addUser.valid) {
            this.successMessage = '';
            this.errorMessage = '';

            this.userService
                .register(
                    this.addUser.get('username').value,
                    this.addUser.get('password').value,
                    this.addUser.get('role').value
                ).subscribe(
                data => (this.getAllUsers()),
                error => (this.errorMessage = error.error.message),
            );
        }
    }

    public openModal() {
        this.errorMessage = '';
        this.modalStatus = true;
        this.addUser.controls['username'].setValue('');
        this.addUser.controls['password'].setValue('');
        this.addUser.controls['role'].setValue('user');
        this.initFormBuilder();
    }

    public openDeleteModal(user: string) {
        this.deleteConfirm = true;
        this.currentUser = user;
    }

    public deleteUser() {
        this.deleteConfirm = false;
        this.userService.delete(this.currentUser).subscribe(
            data => (this.getAllUsers()),
            error => (console.log(error.message)),
        );
    }

    public modifyUser(username: string) {
        this.router.navigate(['profile/' + username]).then();
    }
}
