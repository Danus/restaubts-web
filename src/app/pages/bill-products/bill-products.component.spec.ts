import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillProductsComponent } from './bill-products.component';

describe('BillProductsComponent', () => {
  let component: BillProductsComponent;
  let fixture: ComponentFixture<BillProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
