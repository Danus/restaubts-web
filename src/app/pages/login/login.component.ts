import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {JwtHelper} from '../../jwt.helper';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private loginSubscription: Subscription;
  public errorMessage: string;

  public loginCred = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required),
  });

  constructor(
      private cookieService: CookieService,
      private router: Router,
      private userService: UserService
  ) {}

  ngOnInit(): void {
    const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));

    // if user already logged in, redirect to home
    if (token.username !== undefined) {
      this.router.navigateByUrl('/').then();
    }
  }

  ngOnDestroy(): void {
    if (this.loginSubscription !== undefined) {
      this.loginSubscription.unsubscribe();
    }
  }

  private loginSuccess(token: any): void {
    const date = new Date();

    // Set cookie expire in 5 days
    date.setTime(date.getTime() + 5 * 24 * 60 * 60 * 1000);
    this.cookieService.set('loginTokenRestauBTS', token.access_token, date);
    this.errorMessage = '';

    // After login redirect to home
    this.router.navigateByUrl('/').then();
  }

  private loginError(error: any): void {
    // Display error message
    this.errorMessage = error.error.message;
  }

  public login(): void {
    if (this.loginCred.valid) {
      this.loginSubscription = this.userService.login(
          this.loginCred.get('username').value,
          this.loginCred.get('password').value
      ).subscribe(
          data => this.loginSuccess(data),
          error => this.loginError(error),
      );
    } else {
      this.errorMessage = 'Enter your username or/and password.';
    }
  }

}
