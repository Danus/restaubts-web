import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JwtHelper} from '../../jwt.helper';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public username: string;
  public profileMode: string;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private cookieService: CookieService
  ) {}

  ngOnInit(): void {
    const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenRestauBTS'));

    // if user is not logged in, redirect to login
    if (token.username === undefined) {
      this.router.navigateByUrl('/login').then();
    } else {
      // Get the username by a url parameter
      this.username = this.route.snapshot.paramMap.get('username');
      if (this.username === null) {
        this.profileMode = 'settings';
      } else {
        this.profileMode = 'management';
      }
    }
  }

}
