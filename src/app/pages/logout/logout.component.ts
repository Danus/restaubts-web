import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})

export class LogoutComponent implements OnInit {

  constructor(private cookieService: CookieService, private router: Router) { }

  ngOnInit() {
    // delete login cookie
    this.cookieService.delete('loginTokenRestauBTS');

    // redirect to home after 1 second
    setTimeout(() => {
      this.router.navigateByUrl('/').then();
    }, 500);
  }
}
