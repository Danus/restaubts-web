import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MaterialModule} from './material.module';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TransferHttpCacheModule} from '@nguniversal/common';

import {NavigationComponent} from './components/navigation/navigation.component';
import {CookieService} from 'ngx-cookie-service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {JwtInterceptor} from './jwt.interceptor';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {LandingComponent} from './pages/landing/landing.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './pages/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {LogoutComponent} from './pages/logout/logout.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {ProfileSettingsComponent} from './components/profile-settings/profile-settings.component';
import {ProfileManagementComponent} from './components/profile-management/profile-management.component';
import {UsersComponent} from './pages/users/users.component';
import {UserTableComponent} from './components/user-table/user-table.component';
import { TablesComponent } from './pages/tables/tables.component';
import { TableTableComponent } from './components/table-table/table-table.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductTableComponent } from './components/product-table/product-table.component';
import { BillsComponent } from './pages/bills/bills.component';
import { BillTableComponent } from './components/bill-table/bill-table.component';
import { BillProductsComponent } from './pages/bill-products/bill-products.component';
import { BillProductTableComponent } from './components/bill-product-table/bill-product-table.component';

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        LandingComponent,
        LoginComponent,
        LogoutComponent,
        ProfileComponent,
        ProfileSettingsComponent,
        ProfileManagementComponent,
        UsersComponent,
        UserTableComponent,
        TablesComponent,
        TableTableComponent,
        ProductsComponent,
        ProductTableComponent,
        BillsComponent,
        BillTableComponent,
        BillProductsComponent,
        BillProductTableComponent,
    ],
    imports: [
        HttpClientModule,
        BrowserModule.withServerTransition({appId: 'serverApp'}),
        TransferHttpCacheModule,
        AppRoutingModule,
        MaterialModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
    ],
    providers: [
        CookieService,
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
