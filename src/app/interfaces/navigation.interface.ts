export interface NavItem {
    name: string;
    path: string;
    icon: string;
}

export interface NavDropdown {
    name: string;
    icon: string;
    items: NavItem[];
}
