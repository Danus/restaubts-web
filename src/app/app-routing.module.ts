import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {LogoutComponent} from './pages/logout/logout.component';
import {LandingComponent} from './pages/landing/landing.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {UsersComponent} from './pages/users/users.component';
import {TablesComponent} from './pages/tables/tables.component';
import {ProductsComponent} from './pages/products/products.component';
import {BillsComponent} from './pages/bills/bills.component';
import {BillProductsComponent} from './pages/bill-products/bill-products.component';


const routes: Routes = [
    {
        path: '',
        component: LandingComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'logout',
        component: LogoutComponent,
    },
    {
        path: 'profile',
        component: ProfileComponent,
    },
    {
        path: 'profile/:username',
        component: ProfileComponent,
    },
    {
        path: 'users',
        component: UsersComponent,
    },
    {
        path: 'tables',
        component: TablesComponent,
    },
    {
        path: 'products',
        component: ProductsComponent,
    },
    {
        path: 'bills',
        component: BillsComponent,
    },
    {
        path: 'bill-products/:billId',
        component: BillProductsComponent,
    },
    {
        path: '**',
        redirectTo: '',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
