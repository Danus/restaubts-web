CREATE DATABASE IF NOT EXISTS restauBTS;
USE restauBTS;

CREATE TABLE `tblBill`
(
    `idBill`  int(10) UNSIGNED NOT NULL,
    `fiTable` int(10) UNSIGNED NOT NULL
);

CREATE TABLE `tblBillHasProduct`
(
    `id`         datetime              NOT NULL DEFAULT current_timestamp(),
    `fiBill`     int(10) UNSIGNED      NOT NULL,
    `fiProduct`  int(10) UNSIGNED      NOT NULL,
    `dtQuantity` mediumint(8) UNSIGNED NOT NULL
);

CREATE TABLE `tblProduct`
(
    `idProduct`  int(10) UNSIGNED                        NOT NULL,
    `dtName`     varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `dtCategory` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `dtValue`    double(5, 2) UNSIGNED                   NOT NULL,
    `dtImage`    varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
);

INSERT INTO `tblProduct` (`idProduct`, `dtName`, `dtCategory`, `dtValue`, `dtImage`)
VALUES (13, 'Fanta', 'Cold Drinks', 1.50, 'https://www.stickpng.com/assets/images/580b57fbd9996e24bc43c10f.png'),
       (14, 'Coca Cola', 'Cold Drinks', 1.50,
        'https://www.freepngimg.com/thumb/coca%20cola/1-coca-cola-can-png-image-thumb.png'),
       (15, 'Sprite', 'Cold Drinks', 1.50,
        'https://static.wixstatic.com/media/2cd43b_d6bb3482c484483fbdbf46b7d0c8b072~mv2.png?dn=2cd43b_d6bb3482c484483fbdbf46b7d0c8b072~mv2.png'),
       (16, 'Small Water', 'Cold Drinks', 1.20,
        'https://www.freepnglogos.com/uploads/water-bottle-png/download-water-bottle-png-transparent-image-and-clipart-31.png'),
       (17, 'Big Water', 'Cold Drinks', 1.70,
        'https://www.freepnglogos.com/uploads/water-bottle-png/download-water-bottle-png-transparent-image-and-clipart-31.png'),
       (18, 'Coffee', 'Hot Drinks', 2.50, 'https://toppng.com/uploads/preview/coffee-png-11552943130jtociohvmf.png'),
       (19, 'Latte Machiatto', 'Hot Drinks', 2.70,
        'https://pngimage.net/wp-content/uploads/2018/06/latte-macchiato-png-1.png'),
       (20, 'Cappuchino', 'Hot Drinks', 2.70,
        'https://www.dolce-gusto.de/media/wysiwyg/content_images/sorten-lp-2016/tmp1xd/cappuccino/cappuccino-intenso-zubereitung.png'),
       (21, 'Expresso', 'Hot Drinks', 2.50,
        'https://pngimage.net/wp-content/uploads/2018/05/caf%C3%A9-expresso-png-8.png'),
       (22, 'Ice Cream', 'Desserts', 5.20, 'https://www.stickpng.com/assets/images/580b57fcd9996e24bc43c1bd.png'),
       (23, 'Tiramisu', 'Desserts', 8.20, 'https://www.s.pizza/app/Images/dishes-tiramisu-178067.png'),
       (24, 'Brownie', 'Desserts', 6.10,
        'https://img.pngio.com/brownies-png-100-images-in-collection-page-1-brownies-png-500_500.png'),
       (25, 'Wine 50cl', 'Cold Drinks', 4.50,
        'https://static.wixstatic.com/media/2cd43b_4b785830509b422e8d3a3b6973d5c1e9~mv2_d_1558_1600_s_2.png?dn=2cd43b_4b785830509b422e8d3a3b6973d5c1e9~mv2_d_1558_1600_s_2.png'),
       (26, 'Beer', 'Cold Drinks', 2.10, 'https://www.stickpng.com/assets/images/580b57fbd9996e24bc43c099.png'),
       (27, 'Scampi', 'Appetizers', 6.10, 'https://pngimage.net/wp-content/uploads/2018/06/scampi-png-1.png'),
       (28, 'Smoked Salmon', 'Appetizers', 5.60, 'https://i.ya-webdesign.com/images/salmon-png-17.png'),
       (29, 'Garlic Bread', 'Appetizers', 4.30, 'https://i.dlpng.com/static/png/6745931_preview.png'),
       (30, 'Soup of the day', 'Appetizers', 3.60,
        'https://static.wixstatic.com/media/2cd43b_d78a6bfb87694dc6b42416bceeaaebdb~mv2.png?dn=Soup%20(26).png'),
       (31, 'Salad of the day', 'Appetizers', 3.60, 'https://freepngimg.com/download/salad/9-2-salad-png-image.png'),
       (32, 'Bolognese', 'Main Dishes', 18.60,
        'https://pngimage.net/wp-content/uploads/2018/06/spaghetti-bolognese-png-1.png'),
       (33, 'Grilled Porc Ribs', 'Main Dishes', 14.60,
        'https://cdn.imgbin.com/3/1/3/imgbin-spare-ribs-barbecue-grill-barbecue-sauce-barbecue-chicken-grill-grilled-ribs-PnTbNzHj8EVaD8HQfLKRmV5J3.jpg'),
       (34, 'Fried Chicken', 'Main Dishes', 14.20,
        'https://www.freepnglogos.com/uploads/fried-chicken-png/crispy-fried-chicken-png-0.png'),
       (35, 'Grilled Salmon', 'Main Dishes', 16.20, 'https://i.ya-webdesign.com/images/salmon-png-17.png'),
       (36, 'Grilled Steak', 'Main Dishes', 19.20, 'https://freepngimg.com/download/meat/35944-5-beef-meat-image.png'),
       (37, 'Grilled Porc Belly', 'Main Dishes', 13.20,
        'https://static.wixstatic.com/media/2cd43b_76d7443c687b47c283a11fdb9c3c474d~mv2.png?dn=2cd43b_76d7443c687b47c283a11fdb9c3c474d~mv2.png'),
       (38, 'Cheese Burger', 'Main Dishes', 11.70,
        'https://pngimage.net/wp-content/uploads/2018/05/cheese-burger-png-2.png'),
       (39, 'HamBurger', 'Main Dishes', 10.90,
        'https://i.pinimg.com/originals/c0/ed/55/c0ed55ef7efa3f08a185ce527557c561.png'),
       (40, 'VegiBurger', 'Main Dishes', 10.90,
        'https://www.freepnglogos.com/uploads/burger-png/burger-png-how-healthy-are-vegan-burgers-and-meats-eluxe-magazine-5.png'),
       (41, 'Fries', 'Main Dishes', 3.70,
        'https://www.freepnglogos.com/uploads/fries-png/premium-french-fries-photos-7.png'),
       (42, 'Rice', 'Main Dishes', 3.40, 'https://www.stickpng.com/assets/images/5bbc96d30bc67a02c98d958e.png'),
       (43, 'Menu of the day', 'Menus', 10.20,
        'https://png.pngtree.com/element_our/sm/20180515/sm_f7b6c861bd80a6dfd15d04c4919ff73e.png'),
       (44, 'Kids Menu', 'Menus', 8.60,
        'https://crispy-friedchicken.com/wp-content/uploads/2019/08/kids_wings-1-300x300.png'),
       (45, 'Special Season Menu', 'Menus', 18.90,
        'https://static.wixstatic.com/media/2cd43b_bf6afd1bd63f47a28693cf32708c3d3b~mv2.png?dn=2cd43b_bf6afd1bd63f47a28693cf32708c3d3b~mv2.png');

CREATE TABLE `tblTable`
(
    `idTableNr`      int(11) UNSIGNED                        NOT NULL,
    `dtReservedName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
    `dtIsOccupied`   tinyint(1)                              NOT NULL DEFAULT 0,
    `dtPlaces`       tinyint(3) UNSIGNED                     NOT NULL
);


INSERT INTO `tblTable` (`idTableNr`, `dtReservedName`, `dtIsOccupied`, `dtPlaces`)
VALUES (1, '', 0, 4),
       (2, '', 0, 2),
       (3, '', 0, 6),
       (4, '', 1, 6),
       (5, '', 1, 4),
       (6, '', 0, 4),
       (7, '', 0, 2),
       (8, '', 0, 2),
       (9, '', 0, 4),
       (10, '', 0, 4);

CREATE TABLE `tblUser`
(
    `idUserName` varchar(50) COLLATE utf8mb4_unicode_ci  NOT NULL,
    `dtPassword` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `dtSalt`     varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `dtRole`     varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
);

CREATE TABLE `tblUserHasTable`
(
    `fiUser`  varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
    `fiTable` int(10) UNSIGNED                       NOT NULL
);

ALTER TABLE `tblBill`
    ADD PRIMARY KEY (`idBill`),
    ADD KEY `tblBill_ibfk_1` (`fiTable`);

ALTER TABLE `tblBillHasProduct`
    ADD PRIMARY KEY (`fiBill`, `fiProduct`, `id`) USING BTREE,
    ADD KEY `fkProduct` (`fiProduct`);

ALTER TABLE `tblProduct`
    ADD PRIMARY KEY (`idProduct`);

ALTER TABLE `tblTable`
    ADD PRIMARY KEY (`idTableNr`);

ALTER TABLE `tblUser`
    ADD PRIMARY KEY (`idUserName`);

ALTER TABLE `tblUserHasTable`
    ADD KEY `fk_userTable_user` (`fiUser`),
    ADD KEY `fk_userTable_table` (`fiTable`);

ALTER TABLE `tblBill`
    MODIFY `idBill` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tblProduct`
    MODIFY `idProduct` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `tblBill`
    ADD CONSTRAINT `tblBill_ibfk_1` FOREIGN KEY (`fiTable`) REFERENCES `tblTable` (`idTableNr`) ON UPDATE CASCADE;

ALTER TABLE `tblBillHasProduct`
    ADD CONSTRAINT `fkBill` FOREIGN KEY (`fiBill`) REFERENCES `tblBill` (`idBill`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fkProduct` FOREIGN KEY (`fiProduct`) REFERENCES `tblProduct` (`idProduct`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tblUserHasTable`
    ADD CONSTRAINT `fk_userTable_table` FOREIGN KEY (`fiTable`) REFERENCES `tblTable` (`idTableNr`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_userTable_user` FOREIGN KEY (`fiUser`) REFERENCES `tblUser` (`idUserName`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO restauBTS.tblUser (idUserName, dtPassword, dtRole, dtSalt) VALUES ('root', 'd84fce5a6d2250e35128d54e172cc46f3d6d35d0611d2a0168f78f9b2dfc9debf46e0b55c3c3463590f3df396e33978d8e13f0b4a73bcaf2211381ccada7437e66359c2ad26f63ca9ec97399a73b98c45a77c9738f4eb2987272ed363d152165540c9e76', 'admin', 'ed6d53661508836c35d3e7f969fe34908f5acb8aa92748a506bb3ca3fbe452b1');
