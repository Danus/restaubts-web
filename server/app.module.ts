import {Module} from '@nestjs/common';
import {AngularUniversalModule} from '@nestjs/ng-universal';
import {join} from 'path';
import {AppServerModule} from '../src/main.server';
import {UserController} from './src/user/user.controller';
import {UserModule} from './src/user/user.module';
import {TableModule} from './src/table/table.module';
import {TableController} from './src/table/table.controller';
import {ProductController} from './src/product/product.controller';
import {ProductModule} from './src/product/product.module';
import {BillController} from './src/bill/bill.controller';
import {BillModule} from './src/bill/bill.module';
import {ConfigModule} from '@nestjs/config';

@Module({
    imports: [
        AngularUniversalModule.forRoot({
            bootstrap: AppServerModule,
            viewsPath: join(process.cwd(), 'dist/restau-bts/browser')
        }),
        ConfigModule.forRoot(),
        UserModule,
        TableModule,
        ProductModule,
        BillModule
    ],
    controllers: [UserController, TableController, ProductController, BillController]
})
export class AppModule {
}
