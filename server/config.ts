export const dbCredentials = {
    host: process.env.DBHOST || '192.168.10.10',
    port: process.env.DBPORT || '3306',
    user: process.env.DBUSER || 'homestead',
    password: process.env.DBPASSWORD || 'secret',
    database: process.env.DBNAME || 'restauBTS'
};

export const jwtConstants = {
    secret: 'restauSecretKey32hg5367h534i8rfg'
};
