import {Strategy} from 'passport-local';
import {PassportStrategy} from '@nestjs/passport';
import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {UserService} from './user/user.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly userService: UserService) {
        super({
            usernameField: 'username',
        });
    }

    public async validate(username: string, password: string) {
        const user = await this.userService.validateUser(username, password);
        if (user === undefined) {
            throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
        }
        return user;
    }
}
