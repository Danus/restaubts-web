import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';
import {DBUserDto, UserDto, UserPasswordChangeDto} from './user.dto';
import * as crypto from 'crypto';
import {dbCredentials} from '../../config';
import {createConnection} from 'mysql2/promise';

@Injectable()
export class UserService {

    constructor(private readonly jwtService: JwtService) {
    }

    private connection: createConnection;

    private static sanitizeUser(user: DBUserDto): DBUserDto {
        delete user.dtPassword;
        delete user.dtSalt;
        return user;
    }

    private async connect() {
        this.connection = await createConnection(dbCredentials);
    }

    private async getUserData(username: string): Promise<DBUserDto> {
        await this.connect();
        const [result] = await this.connection.execute(
            'SELECT * FROM tblUser WHERE idUserName = ?', [username]);
        return result[0];
    }

    public async validateUser(username: string, password: string): Promise<DBUserDto> {
        const user: DBUserDto = await this.getUserData(username);

        if (user === undefined) {
            throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
        }

        const hash = crypto.pbkdf2Sync(password, user.dtSalt, 10000, 100, 'sha512').toString('hex');
        if (hash !== user.dtPassword) {
            throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
        }

        return UserService.sanitizeUser(user);
    }

    public async register(userDTO: UserDto) {
        const username: string = userDTO.username;
        const role: string = userDTO.role;
        let password: string = userDTO.password;

        const salt = crypto.randomBytes(32).toString('hex');
        password = crypto.pbkdf2Sync(password, salt, 10000, 100, 'sha512').toString('hex');

        await this.connect();
        try {
            await this.connection.execute(
                'INSERT INTO tblUser (idUserName, dtPassword, dtSalt, dtRole) VALUES (?,?,?,?);',
                [username, password, salt, role]
            );
        } catch {
            throw new HttpException('Duplicate entry', HttpStatus.CONFLICT);
        }
    }


    public async login(userDto: UserDto) {
        const user: DBUserDto = await this.getUserData(userDto.username);
        const payload = {username: userDto.username, role: user.dtRole};
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    public async changePassword(userPasswordChangeDto: UserPasswordChangeDto) {
        const {username, oldPassword} = userPasswordChangeDto;
        let newPassword = userPasswordChangeDto.newPassword;

        await this.connect();

        // verify if old password is correct
        const user: DBUserDto = await this.validateUser(username, oldPassword);

        if (user.idUserName === username) {
            // hash new password
            const salt = crypto.randomBytes(32).toString('hex');
            newPassword = crypto.pbkdf2Sync(newPassword, salt, 10000, 100, 'sha512').toString('hex');

            // update password
            await this.connection.execute(
                'UPDATE tblUser SET dtPassword=?, dtSalt=? WHERE idUserName=?;',
                [newPassword, salt, username]
            );
        }
    }

    public async getUsers() {
        await this.connect();
        const [users] = await this.connection.execute('SELECT idUserName, dtRole from tblUser');
        return users;
    }

    public async getUser(username: string) {
        await this.connect();
        const [users] = await this.connection.execute('SELECT idUserName, dtRole from tblUser WHERE idUserName = ?', [username]);
        return users[0];
    }

    public async delete(username: string) {
        await this.connect();
        try {
            await this.connection.execute(
                'DELETE FROM tblUser WHERE idUserName=?',
                [username]
            );
        } catch {
            throw new HttpException('User not found', HttpStatus.NOT_MODIFIED);
        }
    }

    public async update(userDto: UserDto) {
        const {username, role} = userDto;
        let password = userDto.password;

        await this.connect();

        if (password !== '') {
            // hash new password
            const salt = crypto.randomBytes(32).toString('hex');
            password = crypto.pbkdf2Sync(password, salt, 10000, 100, 'sha512').toString('hex');

            // update password
            await this.connection.execute(
                'UPDATE tblUser SET dtPassword=?, dtSalt=?, dtRole=? WHERE idUserName=?;',
                [password, salt, role, username]
            );
        } else {
            await this.connection.execute(
                'UPDATE tblUser SET dtRole=? WHERE idUserName=?;',
                [role, username]
            );
        }
    }
}
