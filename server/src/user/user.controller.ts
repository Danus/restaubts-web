import {Body, Controller, Delete, Get, Param, Post, UseGuards} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {UserService} from './user.service';
import {UserDto, UserPasswordChangeDto} from './user.dto';

@Controller('user')
export class UserController {
    constructor(private userService: UserService) {
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('register')
    public async register(@Body() requestBody: UserDto) {
        await this.userService.register(requestBody);
    }

    @UseGuards(AuthGuard('local'))
    @Post('login')
    public async login(@Body() requestBody: UserDto) {
        return await this.userService.login(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('changePassword')
    public async changePassword(@Body() requestBody: UserPasswordChangeDto) {
        return await this.userService.changePassword(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:username')
    public async delete(@Param('username') username: string) {
        return await this.userService.delete(username);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('getAll')
    public async getAll() {
        return await this.userService.getUsers();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('get/:username')
    public async get(@Param('username') username: string) {
        return await this.userService.getUser(username);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('update')
    public async update(@Body() requestBody: UserDto) {
        return await this.userService.update(requestBody);
    }
}
