import {Module} from '@nestjs/common';
import {PassportModule} from '@nestjs/passport';
import {UserService} from './user.service';
import {jwtConstants} from '../../config';
import {JwtModule} from '@nestjs/jwt';
import {JwtStrategy} from '../jwt.strategy';
import {LocalStrategy} from '../local.strategy';
import {UserController} from './user.controller';

@Module({
    imports: [
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '5 days'},
        }),
    ],
    providers: [UserService, LocalStrategy, JwtStrategy],
    exports: [UserService],
    controllers: [UserController],
})
export class UserModule {
}
