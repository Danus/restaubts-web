export interface UserDto {
    username: string;
    password: string;
    role: string;
}

export interface DBUserDto {
    idUserName: string;
    dtPassword: string;
    dtSalt: string;
    dtRole: string;
}

export interface UserPasswordChangeDto {
    username: string;
    newPassword: string;
    oldPassword: string;
}
