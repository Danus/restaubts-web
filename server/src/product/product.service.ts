import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {ProductDto} from './product.dto';
import {dbCredentials} from '../../config';
import {createConnection} from 'mysql2/promise';
import {BillService} from '../bill/bill.service';

@Injectable()
export class ProductService {

    constructor(private billService: BillService) {
    }

    private connection: createConnection;

    private async connect() {
        this.connection = await createConnection(dbCredentials);
    }

    public async createProduct(productDto: ProductDto) {
        const {name, category, value, image} = productDto;

        await this.connect();
        await this.connection.execute('INSERT INTO tblProduct (dtName, dtCategory, dtValue, dtImage) VALUES (?,?,?,?);',
            [name, category, value, image]
        );
    }

    public async updateProduct(productDto: ProductDto) {
        const {productNr, name, category, value, image} = productDto;

        await this.connect();
        try {
            await this.connection.execute('UPDATE tblProduct SET dtName=?, dtCategory=?, dtValue=?, dtImage=? WHERE idProduct=?',
                [name, category, value, image, productNr]
            );
        } catch {
            throw new HttpException('Duplicate entry', HttpStatus.CONFLICT);
        }
    }

    public async getProductsByCategory(category: string, search: string) {
        await this.connect();
        const [result] = await this.connection.execute(
            'SELECT * from tblProduct WHERE (dtName LIKE ? OR idProduct LIKE ?) AND dtCategory = ?',
            ['%' + search + '%', '%' + search + '%', category]
        );
        return {products: result};
    }

    public async getProductsByTable(tableNr: number, search: string) {
        const billId = await this.billService.getLatestBill(tableNr);
        if (billId !== undefined) {
            await this.connect();
            const [result] = await this.connection.execute(
                'SELECT DATE_FORMAT(id, "%Y-%m-%d %H:%i:%S") AS id, dtQuantity, idProduct, dtName, dtValue, dtImage ' +
                'FROM tblBillHasProduct, tblProduct WHERE fiBill = ? AND idProduct=fiProduct AND ' +
                '(dtName LIKE ? OR idProduct LIKE ?) ORDER BY id',
                [billId, '%' + search + '%', '%' + search + '%']
            );

            return {products: result};
        } else {
            return {products: []};
        }
    }

    public async getProductsByBill(billId: number) {
        await this.connect();
        const [result] = await this.connection.execute(
            'SELECT dtQuantity, idProduct, dtName, dtValue ' +
            'FROM tblBillHasProduct, tblProduct WHERE fiBill = ? AND idProduct = fiProduct ORDER BY id',
            [billId]
        );

        return result;
    }

    public async getProducts(search: string) {
        await this.connect();
        const [result] = await this.connection.execute('SELECT * from tblProduct WHERE dtName LIKE ? OR idProduct LIKE ?',
            ['%' + search + '%', '%' + search + '%']
        );
        return {products: result};
    }

    public async delete(id: number) {
        await this.connect();
        try {
            await this.connection.execute(
                'DELETE FROM tblProduct WHERE idProduct=?',
                [id]
            );
        } catch {
            throw new HttpException('Product not found', HttpStatus.NOT_MODIFIED);
        }
    }
}
