import {Module} from '@nestjs/common';
import {ProductService} from './product.service';
import {ProductController} from './product.controller';
import {BillService} from '../bill/bill.service';

@Module({
    providers: [ProductService, BillService],
    exports: [ProductService],
    controllers: [ProductController],
})

export class ProductModule {
}
