export interface ProductDto {
    productNr: number;
    name: string;
    category: string;
    value: number;
    image: string;
}

export interface DbProductDto {
    idProduct: number;
    dtName: string;
    dtCategory: string;
    dtValue: number;
    dtImage: string;
}

export interface DbProductBillDto {
    idProduct: number;
    dtName: string;
    dtQuantity: number;
    dtValue: number;
}

export interface ProductArrayDto {
    products: DbProductDto[];
}
