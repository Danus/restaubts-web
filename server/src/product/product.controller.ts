import {Body, Controller, Delete, Get, Param, Post, UseGuards} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {ProductService} from './product.service';
import {ProductDto} from './product.dto';

@Controller('product')
export class ProductController {
    constructor(private productService: ProductService) {
    }

    // @UseGuards(AuthGuard('jwt'))
    @Post('create')
    public async createProduct(@Body() requestBody: ProductDto) {
        await this.productService.createProduct(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('update')
    public async updateProduct(@Body() requestBody: ProductDto) {
        await this.productService.updateProduct(requestBody);
    }

    @Get('getByCategory/:category/:search')
    public async getProductsByCategorySearch(@Param('category') category: string, @Param('search') search: string) {
        return await this.productService.getProductsByCategory(category, search);
    }

    @Get('getByCategory/:category')
    public async getProductsByCategory(@Param('category') category: string) {
        return await this.productService.getProductsByCategory(category, '');
    }

    @Get('getByTable/:tableNr/:search')
    public async getProductsByTableSearch(@Param('tableNr') tableNr: number, @Param('search') search: string) {
        return await this.productService.getProductsByTable(tableNr, search);
    }

    @Get('getByTable/:tableNr')
    public async getProductsByTable(@Param('tableNr') tableNr: number) {
        return await this.productService.getProductsByTable(tableNr, '');
    }

    @Get('getByBill/:billId')
    public async getProductsByBill(@Param('billId') billId: number) {
        return await this.productService.getProductsByBill(billId);
    }

    @Get('getAll/:search')
    public async getProductsSearch(@Param('search') search: string) {
        return await this.productService.getProducts(search);
    }

    @Get('getAll')
    public async getProducts(@Param() search: string) {
        return await this.productService.getProducts('');
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:id')
    public async delete(@Param('id') id: number) {
        return await this.productService.delete(id);
    }
}
