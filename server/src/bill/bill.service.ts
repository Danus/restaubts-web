import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {dbCredentials} from '../../config';
import {createConnection} from 'mysql2/promise';
import {BillDto} from './bill.dto';

@Injectable()
export class BillService {

    constructor() {
    }

    private connection: createConnection;

    private async connect() {
        this.connection = await createConnection(dbCredentials);
    }

    public async getLatestBill(tableNr: number) {
        await this.connect();
        const [result] = await this.connection.execute('SELECT idBill FROM tblBill WHERE fiTable = ? ORDER BY idBill DESC LIMIT 1',
            [tableNr]
        );
        return result[0].idBill;
    }

    public async addProductToBill(billDto: BillDto) {
        const {tableNr, productId, quantity} = billDto;
        const billId = await this.getLatestBill(tableNr);

        await this.connect();
        try {
            await this.connection.execute('INSERT INTO tblBillHasProduct (fiBill, fiProduct, dtQuantity) VALUES (?, ?, ?);',
                [billId, productId, quantity]
            );
        } catch {
            throw new HttpException('Duplicate entry', HttpStatus.CONFLICT);
        }
    }

    public async editProductFromBill(billDto: BillDto) {
        const {id, tableNr, productId, quantity} = billDto;
        const billId = await this.getLatestBill(tableNr);

        await this.connect();
        try {
            await this.connection.execute('UPDATE tblBillHasProduct SET dtQuantity = ? WHERE fiBill=? AND fiProduct=? AND id=?;',
                [quantity, billId, productId, id]
            );
        } catch {
            throw new HttpException('Not Modified', HttpStatus.NOT_MODIFIED);
        }
    }

    public async deleteProductFromBill(billDto: BillDto) {
        const {tableNr, productId, id} = billDto;
        const billId = await this.getLatestBill(tableNr);

        await this.connect();
        try {
            await this.connection.execute('DELETE FROM tblBillHasProduct WHERE fiBill=? AND fiProduct=? AND id=?;',
                [billId, productId, id]
            );
        } catch {
            throw new HttpException('Product does not exist', HttpStatus.NOT_MODIFIED);
        }
    }

    public async getAllBills() {
        await this.connect();
        const [result] = await this.connection.execute('SELECT * FROM tblBill ORDER BY idBill DESC');

        for (let i = 0; i < result.length; i++) {
            const billDetails = await this.getBillDetails(result[i].idBill);
            result[i].qcpTotal = billDetails.qcpTotal;
            result[i].qcpDate = billDetails.qcpDate;
            if (result[i].qcpTotal === 0) {
                delete result[i];
            }
        }

        return result.filter((bill) => {
            return bill != null;
        });
    }

    private async getBillDetails(billId: number) {
        const [result] = await this.connection.execute(
            // tslint:disable-next-line:max-line-length
            'SELECT SUM((dtValue * dtQuantity)) as qcpTotal, DATE_FORMAT(CAST(MIN(CAST(DATE_ADD(id, INTERVAL 2 HOUR) AS SIGNED)) AS DATETIME), "%d/%m/%Y") as qcpDate FROM tblBillHasProduct, tblProduct WHERE fiBill = ? AND idProduct = fiProduct GROUP BY fiBill',
            [billId]
        );

        return result[0] !== undefined ? result[0] : {qcpTotal: 0, qcpDate: 0};
    }
}
