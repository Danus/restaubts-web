import {Body, Controller, Get, Post, UseGuards} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {BillService} from './bill.service';
import {BillDto} from './bill.dto';

@Controller('bill')
export class BillController {
    constructor(private billService: BillService) {
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('addProduct')
    public async addProductToBill(@Body() requestBody: BillDto) {
        await this.billService.addProductToBill(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('editProduct')
    public async editProductFromBill(@Body() requestBody: BillDto) {
        await this.billService.editProductFromBill(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('deleteProduct')
    public async deleteProductFromBill(@Body() requestBody: BillDto) {
        await this.billService.deleteProductFromBill(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('getAll')
    public async getAllBills() {
        return await this.billService.getAllBills();
    }
}
