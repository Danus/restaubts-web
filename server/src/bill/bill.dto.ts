export interface BillDto {
    id: string;
    tableNr: number;
    productId: number;
    quantity: number;
}

export interface DbBillDto {
    idBill: number;
    fiTable: number;
    qcpTotal: number;
    qcpDate: string;
}
