export interface TableDto {
    tableNr: number;
    places: number;
}

export interface TableArrayDto {
    tables: TableDbDto[];
}

export interface TableDbDto {
    idTableNr: number;
    dtPlaces: number;
    dtIsOccupied: boolean;
    dtReservedName: string;
}

export interface TableChangeStatusDto {
    tableNr: number;
    type: string;
    data: string;
}
