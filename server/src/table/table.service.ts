import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {TableChangeStatusDto, TableDto} from './table.dto';
import {dbCredentials} from '../../config';
import {createConnection} from 'mysql2/promise';

@Injectable()
export class TableService {

    constructor() {
    }

    private connection: createConnection;

    private async connect() {
        this.connection = await createConnection(dbCredentials);
    }

    public async createTable (tableDto: TableDto) {
        const {tableNr, places} = tableDto;

        await this.connect();

        try {
            await this.connection.execute('INSERT INTO tblTable (idTableNr, dtPlaces, dtReservedName, dtIsOccupied) VALUES (?,?,?,?);',
                [tableNr, places, '', 0]
            );
        } catch {
            throw new HttpException('Duplicate entry', HttpStatus.CONFLICT);
        }
    }

    public async changeTableStatus (tableChangeStatusDto: TableChangeStatusDto) {
        const {tableNr, type, data} = tableChangeStatusDto;

        await this.connect();

        /*
        * The APPS will change the table status using this request.
        * There are 3 request types. One for changing the occupied status, one for changing the reservation status
        * and the last one for resetting the table once the service finished.
        */

        if (type === 'occupied') {
            await this.connection.execute('UPDATE tblTable SET dtIsOccupied=? WHERE idTableNr=?',
                [data, tableNr]
            );
            await this.connection.execute('INSERT INTO tblBill (fiTable) VALUES (?)',
                [tableNr]
            );
        } else if (type === 'reservation') {
            await this.connection.execute('UPDATE tblTable SET dtReservedName=? WHERE idTableNr=?',
                [data, tableNr]
            );
        } else if (type === 'serviceFinished') {
            await this.connection.execute('UPDATE tblTable SET dtReservedName=?, dtIsOccupied=? WHERE idTableNr=?',
                ['', 0, tableNr]
            );
        }
    }

    public async checkTableStatus (id: number) {
        await this.connect();
        const [result] = await this.connection.execute('SELECT dtIsOccupied FROM tblTable WHERE idTableNr=?', [id]);
        return {isOccupied: result[0].dtIsOccupied};
    }

    public async updateTable (tableDto: TableDto, id: number) {
        const {tableNr, places} = tableDto;

        await this.connect();
        try {
            await this.connection.execute('UPDATE tblTable SET idTableNr=?, dtPlaces=? WHERE idTableNr=?',
                [tableNr, places, id]
            );
        } catch {
            throw new HttpException('Duplicate entry', HttpStatus.CONFLICT);
        }
    }

    public async delete(id: number) {
        await this.connect();
        try {
            await this.connection.execute(
                'DELETE FROM tblTable WHERE idTableNr=?',
                [id]
            );
        } catch {
            throw new HttpException('Table not found', HttpStatus.NOT_MODIFIED);
        }
    }

    public async getTables () {
        await this.connect();
        const [result] = await this.connection.execute('SELECT * FROM tblTable');
        return {tables: result};
    }

    public async getTable (id: number) {
        await this.connect();
        const [result] = await this.connection.execute('SELECT * FROM tblTable WHERE idTableNr = ?',
            [id]);
        return result[0];
    }

    public async getTablesByUser (username: string) {
        await this.connect();
        const [result] = await this.connection.execute(
            'SELECT tblTable.* FROM tblTable, tblUserHasTable WHERE idTableNr = fiTable AND fiUser = ?',
            [username]
        );
        return {tables: result};
    }
}
