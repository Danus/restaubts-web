import {Body, Controller, Delete, Get, Param, Post, UseGuards} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {TableService} from './table.service';
import {TableChangeStatusDto, TableDto} from './table.dto';

@Controller('table')
export class TableController {
    constructor(private tableService: TableService) {
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('create')
    public async createTable(@Body() requestBody: TableDto) {
        await this.tableService.createTable(requestBody);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:id')
    public async delete(@Param('id') id: number) {
        return await this.tableService.delete(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('changeStatus')
    public async changeTableStatus(@Body() requestBody: TableChangeStatusDto) {
        await this.tableService.changeTableStatus(requestBody);
    }

    @Get('checkStatus/:id')
    public async checkTableStatus(@Param() params) {
        return await this.tableService.checkTableStatus(params.id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('update/:id')
    public async updateTable(@Body() requestBody: TableDto, @Param() params) {
        await this.tableService.updateTable(requestBody, params.id);
    }

    @Get('getAll')
    public async getTables() {
        return await this.tableService.getTables();
    }

    @Get('get/:id')
    public async getTable(@Param() params) {
        return await this.tableService.getTable(params.id);
    }

    @Get('getAll/:username')
    public async getTablesByUser(@Param() params) {
        return await this.tableService.getTablesByUser(params.username);
    }
}
